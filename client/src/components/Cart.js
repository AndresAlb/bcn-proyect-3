import React , { useContext, useState, useEffect } from 'react' 

const Cart = (props) => {
	const [cart, setCart] = useState([props.carrito]);
	const [cartPrice, setCartPrice] = useState(0);	
	const [showTempCart, setshowTempCart] = useState(1);
	const [feeShip, setFreeShip] = useState("Default not free shiping");

	/*
    useEffect( () => {
      calculateTotal()
    },[])

    const calculateTotal = () => {
    	let total = 0
	    props.carrito.forEach(function(item){
			console.log('price of item:',item.price)
			total += item.price * item.quantity 
			setCartPrice(total);
		})
    }
    */

   	const listCart = props.carrito.map(function(obj, index) {   		
   		return (
      		<div>
		        <img src={obj.image} />
		        <p>{obj.product}</p>
		        <p>Price {obj.price} $</p>
		        <button onClick={() => subtractProductsQuantityCart(index)}> - </button>
		        <span> </span>
		        <button onClick={() => sumProductsQuantityCart(index)}> + </button>
		        {/*<p>*showTempCart*: {showTempCart}</p>*/}
		        <p>
		          Cart Quantity:
		          <strong> {obj.quantyTemp} </strong>
		        </p>
		        <p>
		          Max Quantity: <strong>{obj.quantity}</strong>
		        </p>
		        <button className="delete" onClick={() => handleDeleteItem(index)}>
		          X
		        </button>
	     	</div>
    	);
  	});

  	const handleDeleteItem = index => {
	    setCartPrice(cartPrice - props.carrito[index].price * props.carrito[index].quantyTemp);
	    let templist = props.carrito;
	    templist.splice(index, 1);
	    setCart([...templist]);
	    console.log(props.carrito);
	};

  	const sumProductsQuantityCart = index => {
    	console.log("this cart item name", props.carrito[index].product);
    	console.log("this cart item tempQuantity of: ", props.carrito[index].quantyTemp);
   		if (props.carrito[index].quantyTemp < props.carrito[index].quantity) {
      		setshowTempCart((props.carrito[index].quantyTemp = props.carrito[index].quantyTemp + 1));
      		setCartPrice(cartPrice + props.carrito[index].price);
    	}
  	};

  	const subtractProductsQuantityCart = index => {
    	if (props.carrito[index].quantyTemp > 0) {
      	setshowTempCart((props.carrito[index].quantyTemp = props.carrito[index].quantyTemp - 1));
      	setCartPrice(cartPrice - props.carrito[index].price);
    	}
  	};

  	useEffect(() => {
	    if (cartPrice >= 500) {
	      setFreeShip("Free Shiping!");
	      document.getElementById("colorFreeShip").style.color = "green";
	    } else {
	      setFreeShip("Not free Shiping!");
	      document.getElementById("colorFreeShip").style.color = "red";
	    }
	});

	return (
		<div>
			<div id="cart"> 
				<h2>CART ITEMS:</h2>
		        <p>Total Cost: {Math.round(cartPrice)} €</p>
		        <p id="colorFreeShip">{feeShip}</p>
				<p className="listCart"> {listCart}</p>			        
		        <button onClick={ ()=> cart.history.push({ pathname:"/checkout", state:{cart} } )}>Go checkout </button>
		    </div>
		</div>
	)

}

export default Cart


