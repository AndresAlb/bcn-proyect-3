import React, { useState, useEffect } from 'react';
import UploadImages from './UploadImages'
import Axios from 'axios';
import {URL} from '../config'

const Cloudinary = (props) => {
    const [pictures,setPictures] = useState([])

    useEffect( ()=> {
       fetch_pictures()
    },[])

    const fetch_pictures = async () => {
      try{
        const response = await Axios.get(`${URL}/pictures/get_all`)
        //debugger
        setPictures([...response.data.pictures])
      }catch(error){
        //debugger
      }
    }
    const remove_picture = async (_id,idx) => {
      var r = window.confirm("Please. Confirm to delete the picture");
      if (r == true) {
        try{
          await Axios.delete(`${URL}/pictures/remove/${_id}`)
          const temp = pictures
          temp.splice(idx,1)
          setPictures([...temp])
          alert('Photo deleted')
        }catch(error){
          debugger
        }
      }
      else{
        alert('Photo not deleted')
      }
    }
    return (
      <div className='container'>
        <div className='header'>
          <h2>Manage pictures</h2>
          <UploadImages fetch_pictures={fetch_pictures}/>
        </div>
        <div className='pictures_container'>
          {
            pictures.map( (picture,idx) => {
              return <div key={idx} className='picture_container'>
                        <img alt='example_image' src={picture.photo_url} style={{width: '70%'}}/>
                        <button onClick={()=>remove_picture(picture._id,idx)}>Remove picture</button>
                     </div>
            })
          }
        </div>
      </div>
    );
}

export default Cloudinary