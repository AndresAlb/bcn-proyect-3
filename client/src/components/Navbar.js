import React from 'react'
import { NavLink } from 'react-router-dom'

const Navbar = () => (
   <div className='navbar'>
     <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/"}
      >
        BCN MUSEUM
      </NavLink>

      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/shop"}
      >
        Tickets
      </NavLink>

       <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/send_email"}
      >
        Contact
      </NavLink>

   </div>
)
   
export default Navbar

const styles = {
  active: {
    color: "gray"
  },
  default: {
    textDecoration: "none",
    color: "white"
  }
};
