import React , { useState } from 'react'
import Axios from 'axios' 
import {URL} from '../config'

const Contact = (props) => {
	
	const [ form , setValues ] = useState({
		name     : '',
		email    : '',
		subject  : '',
		message  : ''
	})
	const [ message , setMessage ] = useState('')
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{

          const response = await Axios.post(`${URL}/emails/send_email`,{
          	name:form.name,
          	email:form.email,
          	subject:form.subject,
          	message:form.message
          })
          setMessage(response.data.message)
          if( response.data.ok ){
              setTimeout( ()=> { 
				  props.login(response.data.token)				 
			  },2000)    
          }
          
		}
        catch(error){
        	console.log(error)
        }
	}
	return (
		<div className="generalbg">
			<div className="expoCenter">				
				<form onSubmit={handleSubmit}
		            onChange={handleChange}
		            className='form_container'>

		            <label>Name</label>
		            <input name="name"/>
		         	<label>Email</label>    
			     	<input name="email"/>
				    <label>Subject</label>
				    <input name="subject"/>
				    <label>Message</label>
				    <input className="areatext" name="message"/>
				    <button>Send message</button>
				    <div className='message'><h4>{message}</h4></div>
		       </form>
		    </div>
	    </div>
	)
   
}

export default Contact










