import React from 'react'

const SecretPage = (props) => {

	return <div className='secret_page'>
	          <h1>Manage the museum data</h1>
	          <button onClick={()=>{
	          	  props.history.push('/data');
	          }}>Manage data</button>
	          <h1>Manage the museum photos</h1>
	          <button onClick={()=>{
	          	  props.history.push('/photos');
	          }}>Manage photos</button>
	          <h1>Close season</h1>
	          <button onClick={()=>{
				  props.history.push('/');
				  props.logout()
				  }}>logout</button>
	       </div>
}

export default SecretPage










