import React from 'react'
import axios from 'axios'
import {URL} from '../config'

const UploadImages = props => {

	const uploadWidget = () => {
        window.cloudinary.openUploadWidget({ 
        	cloud_name: process.env.REACT_APP_CLOUD_NAME, 
        	upload_preset: process.env.REACT_APP_UPLOAD_PRESET, 
			tags:['user']
        },(error, result)=> {
				    //debugger
                if(error){
					//debugger
                }else{
					upload_picture(result)			  
                }
            });
	}
	
	const upload_picture = async (result) => {
        try{ 
			const response = await axios.post(`${URL}/pictures/upload`,{	
																				photo_url:result[0].secure_url, 
																				public_id:result[0].public_id
																			})
			response.data.ok 
			? await props.fetch_pictures()
			: alert('Something went wrong')
		}catch(error){
			debugger
		}
	}


		return (
			<div className="flex_upload">
                <div className="upload">
					<button className ="button"
                    	onClick={uploadWidget} > Upload image
                    </button>
                </div>
            </div>
		)
}

export default UploadImages
