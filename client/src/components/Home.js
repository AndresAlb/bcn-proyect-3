import React , { useState } from 'react'
import Iframe from 'react-iframe'

const Home = () => {
	
	return (		
		<div>
			<div className="homebg"></div>
			<div class="wellcomebg">
				<div className="center">
						<h1>Museum Proyect BCN</h1>
					</div>
				<div className="wellcome">
					<div className="wellImg"> 
						<img src={"https://res.cloudinary.com/ddk15yoet/image/upload/v1580991985/q98hlruyfrfogplkyyfa.png"}	/>
					</div>
					<div className="wellMessag"> 
						<p>The Museo Proyect BCN, since its inaguration in 1819 and throughout its centennial history, has fulfilled the high mission of preserving, exhibiting and enriching all collections and works of art that, closely linked to the history of BCN, constitute one of the highest manifestations of artistic expression of recognized universal value. </p>
						<ul>
							<li>Opening times: Mon-Sun, 9:30-18:30</li>
							<li>Children under 5 years old have free access</li>
						</ul>
					</div>
				</div>
			</div>
			<div className="bg1">
				<div className="expoCenter">
					<p className="expoHome">
						<a href="/colection1">
							<h2>El Gabinete de descanso de Sus Majestades</h2>
							<br></br>
							<p>dsfsdfdsfdsfd</p>
							<br></br>
							<p>dsfsdfdsfdsfd</p>
							<br></br>
							<p>dsfsdfdsfdsfd</p>
						</a>
					</p>				
				</div>
			</div>
			<div className="bg2">
				<div className="expoCenter">
					<p className="expoHome">
						<a href="/colection1">
							<h2>Angel Maeso 1830</h2>
							<br></br>
							<p>dsfsdfdsfdsfd</p>
							<br></br>
							<p>dsfsdfdsfdsfd</p>
							<br></br>
							<p>dsfsdfdsfdsfd</p>
						</a>
					</p>				
				</div>
			</div>			
			<div>
				<Iframe url="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2992.687926404439!2d2.185758350427954!3d41.40258320291519!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a2fe99f74fb3%3A0xf3b78297d63bf1ff!2sDisseny%20Hub%20Barcelona!5e0!3m2!1ses!2ses!4v1580291259579!5m2!1ses!2ses"
		        width="99.7%"
		        height="350px"
		        display="initial"
		        position="relative"/>
		    </div>			
		</div>
	)
}

export default Home

