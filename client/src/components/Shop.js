import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import Cart from "./Cart.js";

const products = [
  {
    product: "Adult ticket",
    price: 10,
    category: "adult",
    bestSeller: false,
    image:
      "https://image.shutterstock.com/image-vector/ticket-museum-coupon-event-entry-260nw-690454279.jpg",
    onSale: true,
    quantity: 10
  },
  {
    product: "Child ticket",
    price: 7,
    category: "free",
    bestSeller: true,
    image:
      "https://image.shutterstock.com/image-vector/ticket-museum-coupon-event-entry-260nw-690454279.jpg",
    onSale: false,
    quantity: 10
  },
  {
    product: "Season pass",
    price: 250.9,
    category: "free",
    bestSeller: true,
    image:
      "https://image.shutterstock.com/image-vector/ticket-museum-coupon-event-entry-260nw-690454279.jpg",
    onSale: false,
    quantity: 10
  }
];

const Shop = (props) => {
  const [cart, setCart] = useState(
    [] || localStorage.getItem('myValueInLocalStorage') 
  );
   //array products in Cart
  const [cartPrice, setCartPrice] = useState(0); //total price products in Cart
  const [showTempCart, setshowTempCart] = useState(1);
  const [feeShip, setFreeShip] = useState("Default not free shiping");
  //Apply two-way data binding to the input field, which basically takes the value from the user and saves it into the state.
  const [searchTerm, setSearchTerm] = useState("");

  const handleSearchChange = event => {setSearchTerm(event.target.value)};

  const handleAddItemToCart = obj => {
    console.log("click img");
    //en React creamos variables temporales para trabajar
    let tempCart = cart;
    //si este objeto es único (indexOf === -1) lo añadimos al carro
    if (tempCart.indexOf(obj) === -1) {
      //añado la key quantyTemp con valor 1 al objeto
      obj.quantyTemp = 1;
      console.log("muestro cantidad temporal", obj.quantyTemp);
      //1ºrellenamos la array con el objeto
      tempCart.push(obj);
      setCart([...tempCart]);
      console.log("Cart ==> ", cart);
      console.log("Object price:", obj.price);
      //añadimos al hook cartPrice la cantidad
      setCartPrice(cartPrice + obj.price);
      //document.getElementById("hid").style.display = "initial";
    }
  };

  const sumProductsQuantityCart = index => {
    console.log("this cart item name", cart[index].product);
    console.log("this cart item tempQuantity 1", cart[index].quantyTemp);
    if (cart[index].quantyTemp < cart[index].quantity) {
      setshowTempCart((cart[index].quantyTemp = cart[index].quantyTemp + 1));
      setCartPrice(cartPrice + cart[index].price);
    }
  };

  const subtractProductsQuantityCart = index => {
    if (cart[index].quantyTemp > 1) {
      setshowTempCart((cart[index].quantyTemp = cart[index].quantyTemp - 1));
      setCartPrice(cartPrice - cart[index].price);
    }
  };

  useEffect(() => {
    if (cartPrice >= 1) {
      document.getElementById("hid").style.display = "initial";
    } else {
      document.getElementById("hid").style.display = "none";
    }
  });

  useEffect(() => {
    if (cartPrice >= 250) {
      setFreeShip("Pick your gift in the museum hall!");
      document.getElementById("colorFreeShip").style.color = "green";
  
    } else {
      setFreeShip("Free gift for season pass visitors. Buy your pass today!");
      document.getElementById("colorFreeShip").style.color = "red";
      //document.getElementsByClassname("hideCenter").style.display = "none";
    }
  });

  const handleDeleteItem = index => {
    console.log(
      "este es el precio del obj con ese index borrado:",
      cart[index].price
    );
    setCartPrice(cartPrice - cart[index].price * cart[index].quantyTemp);
    let templist = cart;
    templist.splice(index, 1);
    setCart([...templist]);
    console.log(cart);
  };

  const listCart = cart.map(function(obj, index) {
    return (
      <div className="center">
        <img src={obj.image} />
        <p>{obj.product}</p>
        <p>Price {obj.price} $</p>       
          <button className="btnshop" onClick={() => subtractProductsQuantityCart(index)}> - </button>
          <span> </span>
          <button className="btnshop" onClick={() => sumProductsQuantityCart(index)}> + </button>
          {/*<p>*showTempCart*: {showTempCart}</p>*/}
          <p>
            Cart Quantity:
            <strong> {obj.quantyTemp} </strong>
          </p>
          <p>
            Max Quantity: <strong>{obj.quantity}</strong>
          </p>
          <button className="delete" onClick={() => handleDeleteItem(index)}>
            X
          </button>
        </div>
    );
  });

  const listProducts = products.map(function(obj) {
    return (
      <div className="grid2 tickets">
        <img onClick={() => handleAddItemToCart(obj)} src={obj.image} />
        <div>
          <h3 className="left">{obj.product}</h3>
          <p className="left">Price: {obj.price} $</p>
        </div>
      </div>
    );
  });

  const listCategoryProducts = products.map(function(obj) {
    if (obj.category === searchTerm) {
      console.log(obj.product);
      return (
        <div>
          <img onClick={() => handleAddItemToCart(obj)} src={obj.image} />
          <h3>{obj.product}</h3>
          <p>category: {obj.category}</p>
          <p>
            Quantity: <strong>{obj.quantity}</strong>
          </p>
          <h4>Price: {obj.price} $</h4>
        </div>
      );
    }
  });

  return (
    <div>
    <div className="shopbg"></div>
      <div className="shop grid2">
        <div className="listPro">{listProducts}</div>      
        <div id="cart">
          <h3 className="shoph2">CART</h3>
          <div className="listCart"> {listCart}</div>
          <p className="center">Total price: {Math.round(cartPrice)} €</p>          
          <p className="center" id="colorFreeShip">{feeShip}</p>
          <div className="center">
            <button id="hid" className="center" onClick={ ()=>props.history.push({ pathname:"/checkout",state:{cart} }) }>Go checkout </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Shop

/*
        <input
          placeholder="Categories search"
          value={searchTerm}
          onChange={handleSearchChange}
        />
        <p>Show category Search:</p>
        <div className="listSearch">{listCategoryProducts}</div>
*/