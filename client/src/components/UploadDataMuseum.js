import React , { useState } from 'react'
import Axios from 'axios'
import {URL} from '../config'


const Register = (props) => {
	const [ form , setValues ] = useState({
		artName     : '',
		artDesc     : '',
		artAuthor   : '',
		artYear     : '',
		artCategory : ''
	})
	const [ message , setMessage ] = useState('')
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
			const response =  await Axios.post(`${URL}/art/create`,{
	            artName     : form.artName,
			    artDesc     : form.artDesc,
			    artAuthor   : form.artAuthor,
				artYear     : form.artYear,
				artCategory : form.artCategory
	        })
	        setMessage(response.data.message)	        
		}
		catch( error ){
			console.log(error)
		}

	}

	/*
	const fetch_art = async () => {
      try{
        const response = await Axios.get(`${URL}/art/get_all`)
        //debugger
        setValues([...response.data.arts])
      }catch(error){
        //debugger
      }
    }
    */

	const showCreateArt = () =>{
		document.getElementById("createArt").style.display = "block";
		document.getElementById("showArt").style.display = "none";
	}
	const showArt = () =>{
		document.getElementById("createArt").style.display = "none";
		document.getElementById("showArt").style.display = "block";
	}

	return (
			<div className="bgdata">
				<div className="submenu">
					<button onClick={()=> showArt()}> Show all art </button>
					<button onClick={()=> showCreateArt()}> Create art </button>
					<button>  </button>
					<button>  </button>
					<button>  </button>

				</div>
				<div id="showArt">
				
 					<table>
					  <tr>
					    <th>Name</th>
					    <th>Description</th>
					    <th>Author</th>
					    <th>Year</th>
					    <th>Category</th>
					    <th> EDIT </th>
					    <th> TRASH </th>
					  </tr>
					  <tr>
					    <td>Alfreds Futterkiste</td>
					    <td>Maria Anders</td>
					    <td>Germany</td>
					    <td>1944</td>
					    <td>Abstract</td>
					    <th className="actionEdit"> <i class="fa fa-edit"></i> </th>
					    <th className="actionDelete"> <i class="fa fa-trash"></i> </th>
					  </tr>
					  <tr>
					    <td>Centro comercial Moctezuma</td>
					    <td>Francisco Chang</td>
					    <td>Mexico</td>
					  </tr>
					  <tr>
					    <td>Ernst Handel</td>
					    <td>Roland Mendel</td>
					    <td>Austria</td>
					  </tr>
					  <tr>
					    <td>Island Trading</td>
					    <td>Helen Bennett</td>
					    <td>UK</td>
					  </tr>
					  <tr>
					    <td>Laughing Bacchus Winecellars</td>
					    <td>Yoshi Tannamuri</td>
					    <td>Canada</td>
					  </tr>
					  <tr>
					    <td>Magazzini Alimentari Riuniti</td>
					    <td>Giovanni Rovelli</td>
					    <td>Italy</td>
					  </tr>
					</table>

			    </div>			

				<div id="createArt">
					<div className="expoCenter">		
						<form onSubmit={handleSubmit}
				             onChange={handleChange}
				             className='form_container'>
				         <label>Piece of art Name</label>
					     <input name="artName"/>
					     <label>Piece of art Description</label>
					     <input name="artDesc"/>
					     	
					     <label>Piece of art Author</label>
					     <input name="artAuthor"/>
					     <label>Piece of art Year</label>
					     <input name="artYear"/>

					     <label>Piece of art Category</label>
					     <input name="artCategory"/>
					     
					     <button>Create new art object</button>
					     <div className='message'><h4>{message}</h4></div>
				       </form>
				    </div>
				</div>
			    <p> </p>
	        </div>
	       )
}

export default Register