import React , { useState } from 'react'


const Footer = () => {
	
	return (
		<div>
			
<footer class="bggrey">
	<div class="grid3">
		<div class="block">
			<h3>Members</h3>
			<a href="">News </a>
			<a href="">Calendar</a>
			<a href="/login">Login </a>
			<a href="/register">Registrer</a>
		</div>
		<div>
			<h3>Legal</h3>
			<a href="">Cookies</a>
			<a href="">Privacy</a>
			<a href="">Terms and conditions</a>						
		</div>
		<div>
			<h3>Contact</h3>
			<p>Plaça Glòries Catalanes, 38, 08018 Barcelona</p>
			<a class="underline" href="tel:+34 938 325 516">+34 938 325 516</a> 
			<p>Mon-Sun, 9:30-18:30</p>
		</div>
	</div>
	<div class="center">
		<p>Copyright © 2020 Museum | BCS Proyect</p>
		<br></br>
	</div>
</footer>
		</div>
	)
}

export default Footer