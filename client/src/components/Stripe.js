import React from 'react'
import Checkout from '../containers/checkout'
import { StripeProvider, Elements } from 'react-stripe-elements'

const Stripe = (props) => {

    return  <StripeProvider apiKey={process.env.REACT_APP_PK_TEST}>
              <Elements>
                <Checkout {...props}/>
              </Elements>
            </StripeProvider>
}

export default Stripe