import React , { useState , useEffect } from 'react'
import axios from 'axios' 
import {URL} from './config'

import { BrowserRouter as Router , Route , Redirect } from 'react-router-dom'
import Home from './components/Home.js'
import Login from './components/Login.js'
import Register from './components/Register.js'
import SecretPage from './components/SecretPage.js'
import Navbar from './components/Navbar.js'
import Shop from "./components/Shop.js";
import Footer from "./components/Footer.js";
import Contact from "./components/Contact.js";
import Stripe from './components/Stripe.js'
import PaymentSuccess from './containers/payment_success'
import PaymentError from './containers/payment_error'
import Cloudinary from './components/Cloudinary'
import UploadDataMuseum from './components/UploadDataMuseum'
import "./index.css";
import "./App.css";

function App() {
  const [isLoggedIn,setIsLoggedIn] = useState(false)
  const token = JSON.parse(localStorage.getItem('token'))
  
  const verify_token = async () => {
   if( token === null )return setIsLoggedIn(false)
     try{
          const response = await axios.post(`${URL}/users/verify_token`,{token})
          return response.data.ok ? setIsLoggedIn(true) : setIsLoggedIn(false)
     }
     catch(error){
        console.log(error)
     }
  }
  
  useEffect( () => {
     verify_token()
  },[verify_token])


  const login  = (token) => {
     localStorage.setItem('token',JSON.stringify(token)) 
     setIsLoggedIn(true)
  }
  const logout = () => {
     localStorage.removeItem('token');
     setIsLoggedIn(false)
  }

  return (
    <Router>
      <Navbar/>
      <Route exact path='/'            component={Home}   /> 
      <Route exact path='/photos'            component={Cloudinary}   />
      <Route exact path='/data'            component={UploadDataMuseum}   />
      <Route exact path='/shop'        component={Shop} /> 
      <Route exact path='/login'      render={ props => <Login login={login} {...props}/>} />
      <Route exact path='/register'    component={Register}/>
      <Route exact path='/send_email'    component={Contact}/>
      <Route exact path='/checkout'                render={props => <Stripe {...props}/>}/>
      <Route exact path='/payment/success' render={props => <PaymentSuccess {...props}/>}/>
      <Route exact path='/payment/error'   render={props => <PaymentError {...props}/>}/> 
      <Route exact path='/secret-page' render={ props => {
                                            return !isLoggedIn 
                                            ? <Redirect to={'/'}/>
                                            : <SecretPage logout={logout} {...props}/>   
      }}/>
      
      <Footer />
      
    </Router>


  );
}

export default App;

