import React from 'react';
import axios from 'axios'
import { injectStripe } from "react-stripe-elements";
import {URL} from '../config'


const Checkout = (props) => {
//=====================================================================================
//=======================  CALCULATE TOTAL FUNCTION  ==================================
//=====================================================================================  
const calculate_total = () => {
    let total = 0
    props.location.state.cart.forEach( ele => total += (ele.price*ele.quantyTemp))
    return total
}
//=====================================================================================
//=======================  CREATE CHECKOUT SESSION  ===================================
//=====================================================================================     
const createCheckoutSession = async() => {
  console.log('================================================>')
  try{
    const response = await axios.post('${URL}/payment/create-checkout-session')//,{products:props.location.state.cart})
    console.log('response.data =>',response.data)
    return response.data.ok 
    ? (
       localStorage.setItem('sessionId', JSON.stringify(response.data.sessionId)),
       redirect(response.data.sessionId)
      )
    : props.history.push('/payment/error')
    }catch(error){
      props.history.push('/payment/error')
    }
}
//=====================================================================================
//=======================  REDIRECT TO STRIPE CHECKOUT  ===============================
//=====================================================================================
const redirect = (sessionId) => {
    props.stripe.redirectToCheckout({
        // Make the id field from the Checkout Session creation API response
        // available to this file, so you can provide it as parameter here
        // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
        sessionId: sessionId
      }).then(function (result) {
          debugger
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `result.error.message`.
      });
} 
//=====================================================================================
//=====================================================================================
//=====================================================================================

  console.log('props==>>', props)

   return <div className='checkout_container'>
            <div>
              <h3>Before proceding to payment check out everything is correct</h3>
            </div>  
            <div className='products_list'>
                {   props.location.state.cart
                    ? props.location.state.cart.map( (item,idx) => {
                        return (
                                  <div className="shop">
                                    <img src={item.image} />
                                    <p className="center">Name: {item.product}</p>
                                    <p className="center">Price per product: {item.price} €</p>
                                    <p className="center">Quantity: {item.quantyTemp} </p>
                                  </div>
                                )
                    })
                    : null
                }
            </div>    
           
                <div className='total'>
                  <h3>Total : {calculate_total()} €</h3>
                  <button onClick={()=>createCheckoutSession()}>Proceed to payment</button>
                </div>
            
          </div>
}

export default injectStripe(Checkout)

/*
const products = [
    {
      name: "Banana",
      images: ["https://res.cloudinary.com/estefanodi2009/image/upload/v1578491659/images/banana.jpg"],
      quantity: 2,
      amount: 10// Keep the amount on the server to prevent customers from manipulating on client
    },
    {
      name: "Kiwi",
      images: ["https://res.cloudinary.com/estefanodi2009/image/upload/v1578491659/images/kiwi.jpg"],
      quantity: 5,
      amount: 20// Keep the amount on the server to prevent customers from manipulating on client
    },
    {
      name: "Strawberry",
      images: ["https://res.cloudinary.com/estefanodi2009/image/upload/v1578493059/images/strawberry.jpg"],
      quantity: 3,
      amount: 30// Keep the amount on the server to prevent customers from manipulating on client
    }
  ]
  */