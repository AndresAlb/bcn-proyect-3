const URL = window.location.hostname === `localhost`
            ? `http://localhost:5000`
            : `http://142.93.134.191`

export { URL }