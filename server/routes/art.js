const router = require('express').Router();
const controller = require('../controllers/art.controller.js'); 

router.post("/create", controller.create);
router.get('/get_all', controller.get_all);
//router.get("/checkout-session", controller.checkout_session);

module.exports = router