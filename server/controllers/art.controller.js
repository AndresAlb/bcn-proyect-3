const Art = require('../models/art.models'); 

const create = async (req,res) => {
	const { artName , artDesc, artAuthor, artYear, artCategory } = req.body;
	console.log(req.body)
	if(!artName || !artDesc || !artCategory) return res.json({ok:false,message:'All field are required'});
    console.log(artYear)
    try{
        const newArt = {
        	name        : artName,
        	description : artDesc,
        	author      : artAuthor,
			year        : artYear,
			category    : artCategory
        }
        console.log(newArt)
        await Art.create(newArt)
        res.json({ok:true,message:'Successful registered piece of art'})
    }catch( error ){
        res.json({ok:false,error})
    }
}

const get_all = async (req,res) => {
    try{
       const arts = await Art.find({})
       res.json({ok:true,arts})
    }catch(error){
       res.json({ok:false})
    }
} 

module.exports = {create, get_all} //Exportar la función o funciones