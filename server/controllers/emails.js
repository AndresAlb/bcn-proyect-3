const nodemailer = require('nodemailer')

// selecting mail service and authorazing with our credentials
const transport = nodemailer.createTransport({
// you need to enable the less secure option on your gmail account
// https://myaccount.google.com/lesssecureapps?pli=1
	service: 'Gmail',
	auth: {
		user: process.env.NODEMAILER_EMAIL,
		pass: process.env.NODEMAILER_PASSWORD,
	}
});

console.log('transport==>', transport)
const send_email = async (req,res) => {
	
	  const { name , email , subject , message } = req.body
	  if(!name || !email || !subject ||!message ) return res.json({ok:false,message:'All field are required'});
	  //console.log( name , email , subject , message  );
	  const default_subject = 'This is a default subject'
	  const mailOptions = {
		    to: process.env.NODEMAILER_EMAIL,
		    subject: "New message from Musseum contact form: " + name,
		    html: '<p>'+(subject || default_subject)+ '</p><p><pre>' + email + '</pre> <pre>' + message + '</pre></p>'
	 
	   }
      try{
           const response = await transport.sendMail(mailOptions)
           console.log('=========================================> Email sent !!')
           return res.json({on:true,message:'We will be in touch soon!'})
      }
      catch( err ){
           return res.json({ok:false,message:err})
      }
}

module.exports = { send_email }