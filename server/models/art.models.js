const mongoose = require('mongoose');

const artSchema = new mongoose.Schema({
    name:{ 
        type:String, 
        unique:true, 
        required:true 
    },
    description:{ 
        type:String,        
        required:true 
    },
    author:{ 
        type:String,        
        //required:true 
    },	
	year: { 
        type:Number,        
        //required:true 
    },			
	category: {
		type:String,        
        required:true 
	}  
});

module.exports = mongoose.model('art', artSchema);