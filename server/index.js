const app  = require('express')();
const port = process.env.port || 5000;
require('dotenv').config()

const express = require('express');
const path = require('path');
// =============== BODY PARSER SETTINGS =====================
const bodyParser= require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// =============== DATABASE CONNECTION =====================
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
async function connecting(){
    try {
    	//MONGOATLAS, se introduciría despues de 'mongodb:// ...
    	//mongodb+srv://alb:<e1r83b80>@cluster0-cdwpk.mongodb.net/test?retryWrites=true&w=majority
        await mongoose.connect('mongodb://127.0.0.1/auth', { useUnifiedTopology: true , useNewUrlParser: true })
        console.log('*** Connected to the DB ***')
    } catch ( error ) {
        console.log('ERROR: Seems like your DB is not running, please start it up !!!');
    }
}
connecting()  
//================ CORS ================================
const cors = require('cors');
app.use(cors());
// =============== ROUTES ==============================
app.use('/users', require('./routes/users.routes'));
app.use('/emails', require('./routes/emails.js'))
app.use('/payment', require('./routes/payment.route.js'))
app.use('/pictures', require('./routes/pictures.routes'))
app.use('/art', require('./routes/art'))


// the __dirname is the current directory from where the script is running
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../client/build')));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
});

// =============== START SERVER =====================
app.listen(port, () => 
	//console.log('===============================================>>>>>')  
    console.log(`server listening on port ${port}`)
);
